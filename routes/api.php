<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'cors',
], function ($router) {
    // Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});

/* API RUTE ZA USERS */
// Route::middleware('auth:api')->post('regapiuser','Users\ApiRegUsersController@register');
Route::post('regapiuser', 'Users\ApiRegUsersController@register');

Route::post('login', 'Users\ApiLogInUserController@login');
Route::post('reqpassres', 'Users\ApiLogInUserController@reqPassRes');
Route::post('resetpass', 'Users\ApiLogInUserController@setResetPass');
Route::post('refresh', 'Users\ApiLogInUserController@refresh');
Route::middleware('auth:api')->get('users', 'Users\GetUsersController@index');
Route::middleware('auth:api')->put('updateuserinfo/{idUserInfo}', 'UserinfoController@update');
Route::middleware('auth:api')->get('user', 'Users\ApiLogInUserController@user');

/* API RUTE ZA DOCUMENTINFO */
Route::resource('contenschema', 'Contentschema\ContenschemaController', ['except' => ['create', 'edit']]);

/* API RUTE ZA JEZIKE */
Route::resource('lang', 'Settings\LanguageController', ['except' => ['create', 'edit']]);

/* API RUTE ZA TEKSTOVE */
Route::resource('text', 'Text\TextController', ['except' => ['create', 'edit']]);

/* API RUTE ZA LINKOVE */
Route::resource('link', 'Link\LinkController', ['except' => ['create', 'edit']]);
Route::put('linkremovetext/{id}', 'Link\LinkController@removeLinkText')->where('id', '[A-z0-9]+');;
Route::get('linksinmenu/{id}','Link\LinkController@getMenuLinks')->where('id', '[A-z0-9]+');

/*API RUTA ZA SLIKE */
Route::get('images/{filename}', 'Fileupload\FileUploadController@getFileFromStorage');
Route::post('removepivotimage', 'Fileupload\FileUploadController@removePivotImage');
Route::post('addpivotimage', 'Fileupload\FileUploadController@addPivotImage');
Route::post('addseoimage', 'Fileupload\FileUploadController@addSeoImage');

/* API RUTE ZA Menije */
Route::resource('menu', 'Menu\MenuController', ['except' => ['create', 'edit']]);
