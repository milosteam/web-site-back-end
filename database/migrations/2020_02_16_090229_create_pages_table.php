<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->bigIncrements('id_page');
            $table->string('page_name', 60)->unique();
            $table->integer('page_visited')->unsigned()->nullable();
            $table->bigInteger('id_page_seo')->unsigned()->nullable();
            $table->bigInteger('id_social')->unsigned()->nullable();
            $table->bigInteger('id_link')->unsigned()->nullable();
            $table->bigInteger('id_page_content')->unsigned();
            $table->bigInteger('id_user')->unsigned()->nullable();
            $table->boolean('active')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
