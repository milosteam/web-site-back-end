<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileuploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fileuploads', function (Blueprint $table) {
            $table->bigIncrements('idFile');
            $table->string('nameOfFile')->unique();
            $table->string('desktopThumbPath');
            $table->bigInteger('sizeOfFile');
            $table->string('typeOfFile');
            $table->string('directoryOfFile');
            $table->string('mobThumbPath')->nullable();
            $table->string('tabletThumbPath')->nullable();
            $table->smallInteger('accessLevel')->nullable();
            $table->integer('singleUserFile')->nullable();
            $table->text('napomena')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fileuploads');
    }
}
