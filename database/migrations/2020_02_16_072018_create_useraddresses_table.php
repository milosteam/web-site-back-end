<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUseraddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('useraddresses', function (Blueprint $table) {
            $table->bigIncrements('id_user_address');
            $table->unsignedBigInteger('id_user');
            $table->tinyInteger('address_ordinal');
            $table->boolean('primary_address')->default(false);
            $table->string('address_type')->default('universal');
            $table->string('locality', 100);
            $table->string('province', 100);
            $table->char('zip', 20);
            $table->string('country',100);
            $table->timestamps();
            $table->foreign('id_user')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('useraddresses');
    }
}
