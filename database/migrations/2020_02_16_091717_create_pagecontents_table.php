<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagecontentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pagecontents', function (Blueprint $table) {
            $table->bigIncrements('id_page_content');
            $table->string('content_name',60)->unique();
            $table->string('page_template_name', 60);
            $table->bigInteger('id_content_schema')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('id_content_schema')->references('id_content_schema')->on('contentschemas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pagecontents');
    }
}
