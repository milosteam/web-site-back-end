<?php

use App\Traits\Users\USERROLES;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrivateUserinfosTable extends Migration implements USERROLES
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('privateuserinfos', function (Blueprint $table) {
            $table->bigIncrements('id_private_userinfo');
            $table->smallInteger('user_role')->default(USERROLES::USERROLE['user']);
            $table->string('department')->nullable();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('gender', 10)->nullable();
            $table->date('date_of_birth')->nullable();
            $table->binary('phone');
            $table->tinyInteger('rating')->nullable();
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('userinfos');
    }
}
