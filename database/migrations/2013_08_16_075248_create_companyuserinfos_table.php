<?php

use App\Traits\Users\USERROLES;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyuserinfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companyuserinfos', function (Blueprint $table) {
            $table->bigIncrements('id_company_userinfo');
            $table->smallInteger('user_role')->default(USERROLES::USERROLE['user']);
            $table->string('company_name', 100)->unique();
            $table->string('company_type', 50)->nullable();
            $table->string('company_activity', 50)->nullable();
            $table->char('company_activity_code')->nullable();
            $table->char('tin',20)->nullable();
            $table->binary('company_contact_personnel')->nullable();
            $table->binary('phone');
            $table->binary('company_emails');
            $table->tinyInteger('rating')->nullable();
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companyuserinfos');
    }
}
