<?php

use App\Traits\Users\USERROLES;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagelinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('links', function (Blueprint $table) {
            $table->bigIncrements('id_link');
            $table->string('link_name',60)->unique();
            $table->tinyInteger('link_auth')->defaul(USERROLES::USERROLE['user']);
            $table->boolean('isParent')->default(false);
            $table->timestamps();
            $table->bigInteger('id_content_schema')->unsigned()->nullable();
            $table->foreign('id_content_schema')->references('id_content_schema')->on('contentschemas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pagelinks');
    }
}
