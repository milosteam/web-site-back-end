<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGallerycomponentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gallerycomponents', function (Blueprint $table) {
            $table->bigIncrements('id_gallery_component');
            $table->string('gallery_name')->unique();
            $table->char('gallery_type');
            $table->bigInteger('id_content_schema')->unsigned();
            $table->timestamps();
            $table->foreign('id_content_schema')->references('id_content_schema')->on('contentschemas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gallerycomponents');
    }
}
