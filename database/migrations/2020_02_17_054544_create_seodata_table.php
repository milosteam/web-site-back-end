<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeodataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seodata', function (Blueprint $table) {
            $table->bigIncrements('id_seo_data');
            $table->string('page_title',200)->unique();
            $table->string('page_key_words');
            $table->text('page_description');
            $table->binary('view_port');
            $table->binary('page_langs');
            $table->binary('page_geo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seodata');
    }
}
