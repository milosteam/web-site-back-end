<?php

use App\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->boolean('verified')->default(User::VERIFIED['no']);
            $table->string('verification_token')->nullable();
            $table->bigInteger('id_private_userinfo', false)->unsigned()->nullable();
            $table->bigInteger('id_company_userinfo', false)->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('id_private_userinfo')->references('id_private_userinfo')->on('privateuserinfos');
            $table->foreign('id_company_userinfo')->references('id_company_userinfo')->on('companyuserinfos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
