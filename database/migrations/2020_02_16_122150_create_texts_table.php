<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('texts', function (Blueprint $table) {
            $table->bigIncrements('id_text');
            $table->char('text_type', 20);
            $table->string('text_name', 100)->unique()->nullable();
            $table->string('text_title', 100)->nullable()->nullable();
            $table->binary('text_paragraphs');
            $table->bigInteger('id_language')->unsigned();
            $table->bigInteger('id_content_schema')->unsigned()->nullable();
            $table->timestamps();
            $table->foreign('id_language')->references('id_language')->on('languages');
            $table->foreign('id_content_schema')->references('id_content_schema')->on('contentschemas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('textcomponents');
    }
}
