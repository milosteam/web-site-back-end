<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->bigIncrements('id_menu');
            $table->string('menu_name', 60)->unique();
            $table->tinyInteger('menu_position')->unsigned()->nullable();
            $table->timestamps();
            $table->bigInteger('id_content_schema')->unsigned()->nullable();
            $table->foreign('id_content_schema')->references('id_content_schema')->on('contentschemas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pagemenus');
    }
}
