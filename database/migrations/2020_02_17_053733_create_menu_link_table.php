<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenulinkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_link', function (Blueprint $table) {
            $table->bigIncrements('id_menu_link');
            $table->bigInteger('id_menu')->unsigned();
            $table->bigInteger('id_link')->unsigned();
            $table->integer('position');
            $table->tinyInteger('level');
            $table->bigInteger('id_parent_link')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menulinks');
    }
}
