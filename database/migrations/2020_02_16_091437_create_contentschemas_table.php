<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentschemasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contentschemas', function (Blueprint $table) {
            $table->bigIncrements('id_content_schema');
            $table->string('schema_name', 60)->unique();
            $table->tinyInteger('schema_type');
            $table->boolean('is_complex_schema')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contentschemas');
    }
}
