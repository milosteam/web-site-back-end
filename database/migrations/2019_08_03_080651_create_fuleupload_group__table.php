<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFuleuploadGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fileupload_group', function (Blueprint $table) {
            $table->bigIncrements('id_file_group');
            $table->integer('idFile')->unsigned();
            $table->integer('idParent')->unsigned();
            $table->string('parentGroup');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fileupload_oprema');
    }
}
