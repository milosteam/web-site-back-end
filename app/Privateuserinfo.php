<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Userinfo extends Model
{
    protected $primaryKey = "idUserInfo";

    const USERROLE = ['superadmin'=> 5, 'admin'=> 4, 'moderator' => 3, 'user'=>2];

    const DEPARTMENT = ['finansije'=>'fin', 'komercijala'=>'kom', 'opriprema'=>'op', 'tpriprema'=>'tp', 'kontrola' => 'kont',
    'dizajn'=>'dz', 'sforma'=>'sf', 'magacin'=>'mag', 'proizvodnja' => 'prz', 'menadzment'=>'men', 'odrzavanje'=>'odr'];
    
    protected $fillable = [
                'userRole',
                'department',
                'username',
                'phone',
    ];

     // Getters and Setters
      public function setUserRoleAttribute($userRole){
        if ($userRole!== '') {
            foreach(Userinfo::USERROLE as $key=>$value) {
                if (strtolower($userRole)=== $key){
                    $this->attributes['userRole'] = $value;
                }
            };
        }
    }

   
    public function proizvod() {
        return $this->belongsTo(Proizvod::class, 'idProizvoda');
    }
  
    public function user() {
        return $this->belongsTo(User::class);
    }


    public function merenjes()
    {
        return $this->hasMany(Merenje::class, 'idMerenja');
    }
    


}
