<?php

namespace App;

use App\Oprema;
use App\Uredjaj;
use App\Model\Text\Text;

use App\Model\Settings\Language;
use Illuminate\Database\Eloquent\Model;

class Fileupload extends Model
{
    protected $primaryKey = 'idFile';

    protected $fillable = [
        'nameOfFile',
        'mobThumbPath',
        'tabletThumbPath',
        'desktopThumbPath',
        'sizeOfFile',
        'typeOfFile',
        'directoryOfFile',
        'mobThumbPath',
        'tabletThumbPath',
        'accessLevel',
        'singleUserFile',
        'napomena'
];


public function language()
{
    return $this->belongsToMany(Language::class, 'fileupload_group','idFile','idParent')->withPivot('parentGroup');
}

public function text()
{
    return $this->belongsToMany(Text::class, 'fileupload_group','idFile','idParent')->withPivot('parentGroup');
}


}
