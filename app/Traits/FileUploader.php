<?php 
namespace App\Traits;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\Filesystem;

trait FileUploader {

    public function filesAndData(Request $request, $fileData){
        $file=[];
        if ($request->hasFile($fileData['fileProp'])){
            $fileNameExt = $request->file($fileData['fileProp'])->getClientOriginalName();
            $fileName = $this->generateFileName($fileNameExt, $fileData);
            $filePath = $request->file($fileData['fileProp'])->storeAs('public/images/'.$fileData['storeFolder'], $fileName);
            $filePathDB = 'images/'.$fileData['storeFolder'].'/'.$fileName;
            return $this->getFileMetaData($filePathDB, $filePath, $fileName, $fileData);
        } else {
            $this->setDefaultImage();
        };
}

public function multyFileUpload(Request $request, $fileData){
    if ($request->hasFile($fileData['fileProp'])){
        $files = $request->file($fileData['fileProp']);
        foreach($files as $file){
            $fileNameExt = $file->getClientOriginalName();
            $fileName = $this->generateFileName($fileNameExt, $fileData);
            $filePath = $file->storeAs('public/images/'.$fileData['storeFolder'], $fileName);
            $filePathDB = 'images/'.$fileData['storeFolder'].'/'.$fileName;
            $filesMetaData[] = $this->getFileMetaData($filePathDB, $filePath, $fileName, $fileData);
        }
        return $filesMetaData;
    } else {
        return false;
    }
}

private function generateFileName($filename, $data)
{
 $orgFileName = pathinfo($filename, PATHINFO_FILENAME);
 $orgFileEXt = pathinfo($filename, PATHINFO_EXTENSION);
 return $fileNameToStore = $data['filePref'] . '_' . $orgFileName . time() . '.'. $orgFileEXt;
}

private function setDefaultImage(){
    return $file= [
        'nameOfFile'=>'noimage.jpg',
        'mobThumbPath' => 'mob_noimage.jpg',
        'tabletThumbPath' => '',
        'desktopThumbPath' => ''
    ];
}

private function getFileMetaData($filePathDB, $filepath, $filename, $fileData) {
    return $file=[
                  'nameOfFile' => $filename,
                  'desktopThumbPath' => $filePathDB,
                  'sizeOfFile' => Storage::size($filepath),
                  'typeOfFile' => pathinfo($filename, PATHINFO_EXTENSION),
                  'directoryOfFile' => $fileData['filePref'],
                 ];
}

private function generateUploadPath($fileData, $fileName){
    return 'public/images/'.$fileData['storeFolder'].'/'.$fileName;
}

public function getFileStorage ($filename)
{
    $path = storage_path('public/' . $filename);

    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
}

public function deleteFile($fileData, $filename)
{
   $file = $this->generateUploadPath($fileData, $filename);
     if(Storage::exists($file))
        {
            // 1. possibility
            Storage::delete($file);
            // 2. possibility
            // unlink(storage_path('app/folder/'.$file));
            return $file; 
        }
    else
    {
       return $file;
    }
}


}

?>