<?php
namespace App\Traits;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

trait PaginationRequest {

public function paginationVal(Request $request) {
    $header = $request->query();
    if (isset($header['pagination'])) {
        $pag = json_decode($header['pagination'], true);
        $errors = $this->paginationValidator($pag)->errors();
        if (!count($errors)) {
            return [
                'per_page' => $pag['per_page'],
                'page' => $pag['page'],
            ];
        }
    }
}

protected function paginationValidator(array $data)
{
    return Validator::make($data, [
        'per_page' => 'required|integer',
        'page' => 'required|integer'
     ]);
}

}


?>