<?php
namespace App\Traits;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

trait ApiResponser {

        

        public function successResponse($data, $code) 
        {
            return response()->json(['data' => $data], $code);
        }

        public function deleteResponse($data = false, $code = 403){
            return response()->json(['data' => $data], $code);
        }

        protected function errorResponse($message, $code)
        {
            return response()->json(['error' => $message, 'code' => $code], $code);
        }

        protected function showAll(String $collectioName = 'data', Collection $collection, $code = 200) {
            return $this->successResponse([$collectioName => $collection], $code);   
        }

        protected function showOne(String $collectioName = 'data', Model $model, $code = 200)
        {
            return $this->successResponse([$collectioName => $model], $code);            
        }

}


?>