<?php 

namespace App\Classes\Validation;
class Validationexpr {

    private static $linetextval = 'regex:/^([a-zA-ZšŠđĐžŽčČćĆ10-9\s\-,]{3,})$/';
   
    public static function getLineTextVal(): string {
        return self::$linetextval;
    }
}

?>