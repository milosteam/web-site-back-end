<?php

namespace App\Model\Link;

use App\Model\Menu\Menu;
use App\Model\Text\Text;
use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $primaryKey = "id_link";

    const USERROLE = ['superadmin'=> 5, 'admin'=> 4, 'moderator' => 3, 'user'=>2];

    const LINKDATA =  ['fileProp' => 'katImage', 'filePref' => 'link', 'storeFolder' => 'settings/link'];

    protected $fillable = [
        'link_name',
        'link_auth',
    ];

    // Getters and Setters
    public function setLinkAuthAttribute($link_auth){
        if ($link_auth!== '') {
            foreach(self::USERROLE as $key=>$value) {
                if (strtolower($link_auth)=== $key){
                    $this->attributes['link_auth'] = $value;
                }
            };
        }
    }



    public function texts()
    {
        return $this->belongsToMany(Text::class, 'text_group', 'idParent', 'id_text')->withPivot('parentGroup')->wherePivot('parentGroup', self::LINKDATA['filePref']);
    }

    public function menu()
    {
        return $this->belongsToMany(Menu::class, 'menu_link', 'id_link', 'id_menu')->withPivot('position','level','id_parent_link')->wherePivot('id_parent_link', 0);
    }

    public function sublinks(){
        return $this->belongsToMany(Link::class, 'menu_link', 'id_parent_link', 'id_link')->withPivot('position','level','id_parent_link','id_menu');
    }

   
   
}
