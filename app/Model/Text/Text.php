<?php

namespace App\Model\Text;

use App\Model\Link\Link;
use Illuminate\Database\Eloquent\Model;

class Text extends Model
{
    protected $primaryKey = 'id_text';

    const TEXTDATA =  ['fileProp' => 'katImage', 'filePref' => 'text', 'storeFolder' => 'settings/text'];

    protected $fillable = [
        'text_type',
        'text_name',
        'text_title',
        'text_paragraphs',
        'id_language',
        'id_content_schema'
    ];

    public function link()
    {
        return $this->belongsToMany(Link::class, 'text_group', 'id_text', 'idParent')->withPivot('parentGroup');
    }

    public function files()
    {
        return $this->belongsToMany('App\Fileupload', 'fileupload_group', 'idParent', 'idFile')->withPivot('parentGroup')->wherePivot('parentGroup', self::TEXTDATA['filePref']);;
    }
}
