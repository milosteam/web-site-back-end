<?php

namespace App\Model\Settings;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $primaryKey = 'id_language';

    const LANGUGAEDATA =  ['fileProp' => 'katImage', 'filePref' => 'language', 'storeFolder' => 'settings/language'];
    
    protected $fillable = [
        'language',
        'identifier'
    ];

    public function files()
    {
        return $this->belongsToMany('App\Fileupload', 'fileupload_group','idParent','idFile')->withPivot('parentGroup')->wherePivot('parentGroup', self::LANGUGAEDATA['filePref']);;
    }
}
