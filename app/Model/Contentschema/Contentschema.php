<?php

namespace App\Model\Contentschema;

use Illuminate\Database\Eloquent\Model;


class Contentschema extends Model
{
    protected $primaryKey = 'id_content_schema';

    const ISACTIVE = 1;
    const ISNOTACTIVE = 0;

    protected $fillable = [
        'schema_name',
        'schema_type',
        'is_complex_schema',
    ];

    const CONTENTSCHEMATYPE = ['page' => 1, 'section' => 2, 'component' => 3];

    public function components()
    {
        return $this->belongsToMany(Contentschema::class, 'contentcomponents', 'id_sub_schema', 'id_parent_schema')->withPivot('parentGroup');
    }


    public function setSchemaTypeAttribute($schema_type)
    {
        if ($schema_type !== '') {
            foreach (self::CONTENTSCHEMATYPE as $key => $value) {
                if (strtolower($schema_type) === $key) {
                    $this->attributes['schema_type'] = $value;
                }
            };
        }
    }

    public function getSchemaTypeAttribute($schema_type)
    {
        if ($schema_type !== '') {
            foreach (self::CONTENTSCHEMATYPE as $key => $value) {
                if ($schema_type === $value) {
                    return $key;
                }
            };
        }
    }
}
