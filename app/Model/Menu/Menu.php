<?php

namespace App\Model\Menu;

use App\Model\Link\Link;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected  $primaryKey = 'id_menu';

    protected $fillable =
    [
        'menu_name',
        'menu_position',
    ];

    public function links()
    {
        return $this->belongsToMany(Link::class, 'menu_link', 'id_menu', 'id_link')->withPivot('position','level','id_parent_link');
    }

    public function sublinks(){

    }

    // public pages(){}
}
