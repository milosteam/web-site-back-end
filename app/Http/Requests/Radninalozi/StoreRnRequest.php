<?php

namespace App\Http\Requests\Radninalozi;

use Illuminate\Foundation\Http\FormRequest;

class StoreRnRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'radniNalogPanth' => 'required',
            'idProizvoda' => 'required'
        ];
    }
}
