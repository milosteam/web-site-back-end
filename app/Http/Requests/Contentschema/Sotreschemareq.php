<?php

namespace App\Http\Requests\Contentschema;

use Illuminate\Foundation\Http\FormRequest;
use App\Classes\Validation\Validationexpr;

class Sotreschemareq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'schema_name'=>'bail|required|'. Validationexpr::getLineTextVal(),
            'schema_type'=>'required',
            'is_complex_schema'=>'required',
        ];
    }
}

