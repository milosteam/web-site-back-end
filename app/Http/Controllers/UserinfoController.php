<?php

namespace App\Http\Controllers;

use App\Userinfo;
use Illuminate\Http\Request;
use App\Traits\PaginationRequest;
use App\Http\Controllers\ApiController;

class UserinfoController extends ApiController
{
    use PaginationRequest;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Userinfo  $userinfo
     * @return \Illuminate\Http\Response
     */
    public function show(Userinfo $userinfo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Userinfo  $userinfo
     * @return \Illuminate\Http\Response
     */
    public function edit(Userinfo $userinfo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Userinfo  $userinfo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $idUserInfo)
    {
        $data = $request->all();
        $userinfo = Userinfo::where('idUserInfo','LIKE', $idUserInfo)->first();
        if($userinfo) {
            foreach ($data as $key => $value){
                if ($userinfo[$key] !== $value ){
                    $userinfo[$key] = $value;
                }
            }
            if( !$userinfo->isDirty()){
                return response()->json(['error' => 'Nista od podataka nije izmenjeno','code' => 304], 403);
            }
            else {
                $userinfo->save();
                return $this->successResponse($userinfo, 201);
            }
            return $this->successResponse($userinfo, 201);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Userinfo  $userinfo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Userinfo $userinfo)
    {
        //
    }
    
}
