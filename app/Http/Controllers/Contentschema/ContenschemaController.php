<?php

namespace App\Http\Controllers\Contentschema;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use App\Http\Controllers\ApiController;
use App\Model\Contentschema\Contentschema;
use App\Http\Requests\Contentschema\Sotreschemareq;

class ContenschemaController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $pag = $this->paginationVal($request);
            $page = $pag['page'];
            Paginator::currentPageResolver(function () use ($page) {
                return $page;
            });
            $header = $request->query();
            if (isset($header['filter'])) {
                $filt = json_decode($header['filter'], true);
                $query = $this->filterQuery($filt);
                if ($pag) {
                    return ($this->successResponse($query->orderBy('updated_at', 'DESC')->paginate($pag['per_page']), 200));
                } else {
                    return ($this->successResponse($query->orderBy('updated_at', 'DESC')->paginate($pag['per_page']), 200));
                }
            } else {
                if ($pag) {
                    $contentscheams = Contentschema::orderBy('updated_at', 'DESC')->paginate($pag['per_page']);
                } else {
                    $contentscheams = Contentschema::orderBy('updated_at', 'DESC')->get();
                }
                return ($this->successResponse($contentscheams, 200));
            }
        } catch (Exception $e) {
            return $this->errorResponse('Došlo je do greške.', 403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Sotreschemareq $request)
    {
        $request->validated();
        $data = $request->all();
        $contentschema = Contentschema::create($data);
        if (isset($contentschema)) {
            return $this->successResponse($contentschema, 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Sotreschemareq $request, $id)
    {
        $request->validated();
        $contentschema = Contentschema::findOrFail($id);
        $data = $request->all();
        foreach ($data as $key => $value) {
            if (array_key_exists($key, $contentschema->toArray()) && $contentschema->$key !== $value) {
                $contentschema->$key = $value;
            }
        }
        if (!$contentschema->isDirty()) {
            return $this->errorResponse('Nista od podataka nije izmenjeno', 422);
        } else {
            $contentschema->save();
            return $this->successResponse('Uspešno ste izmenili plan / merenje', 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
