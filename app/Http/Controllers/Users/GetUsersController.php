<?php 
namespace App\Http\Controllers\users;

use App\User;
use App\Userinfo;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use App\Traits\PaginationRequest;
use Illuminate\Pagination\Paginator;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Validator;


class GetUsersController extends ApiController
{
    use PaginationRequest;

    // public function __construct()
    // {
    //     $this->middleware('auth:api');
    // }

    public function index(Request $request)
    {
        $pag = $this->paginationVal($request);
        $page = $pag['page'];
        Paginator::currentPageResolver(function () use ($page) {
            return $page;
        });

        $users = Userinfo::paginate($pag['per_page']);

        return $this->successResponse($users, 200);
    }

    public function updateUser(Request $request) {
        
    }

}


?>