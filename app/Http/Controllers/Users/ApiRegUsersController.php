<?php

namespace App\Http\Controllers\users;

use App\User;
use App\Userinfo;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Auth\RegisterController;

class ApiRegUsersController extends RegisterController
{
    use ApiResponser;

    protected function apiUserValidator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
         ]);
    }


    protected function apiUserInfoValidator(array $data){
        return Validator::make($data,[
            'username' => 'required|string',
            'department' => 'required|string',
            'userRole' => 'required'
        ]);
    }

    public function register(Request $request) {
            $data = $request->all();
            if ($data['user'] && $data['userinfo']){
                $errors = $this->apiUserInfoValidator($data['userinfo'])->errors();
                if (count($errors)){
                    return  $this->errorResponse('Uneti podaci nisu korektni.', 403); 
                }
                else {
                $userinfo = Userinfo::create($data['userinfo']);
                $data['user']['idUserInfo'] = $userinfo['idUserInfo'];
                $user = User::create([
                    'name' => $data['user']['email'],
                    'email' => $data['user']['email'],
                    'password' => bcrypt($data['user']['password']),
                    'idUserInfo' =>  $data['user']['idUserInfo']
                ]);
                return $this->successResponse($userinfo, 201); 
            }
          }
    }

}
