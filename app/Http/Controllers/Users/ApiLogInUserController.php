<?php

namespace App\Http\Controllers\users;

use App\User;
use App\Userinfo;
use App\Mail\ResetPassMail;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Proxy\LoginProxy;


class ApiLogInUserController extends LoginProxy {

use ApiResponser;
    //
public function login(Request $request) {
    $data = $request->all();
    $errors = $this->apiLoginValidator($data)->errors();
    if (count($errors)){
        return $this->successResponse($errors, 403);
    }
    else {
        $user = $this->attemptLogin($data['email'], $data['password']);
        if($user){
            return $this->successResponse($user, 200);
        }
    }

    // return $this->successResponse($data, 200);
}

public function refresh(Request $request) {
    $data = $this->attemptRefresh($request);
    if(isset($data)) {
        return $this->successResponse($data, 200);
    }
}

public function user(Request $request) {
    $user = $request->user();
    $userinfo = Userinfo::find($user->idUserInfo);
    return $this->successResponse(['userinfo' => $userinfo], 200);
}


public function setResetPass(Request $request) {
    $data = $request->all();
    $errors = $this->apiResetPassValidator($data)->errors();
    if(count($errors)){
        return $this->successResponse($errors, 403);
    }
    else {
        if (DB::table('password_resets')->where([
            ['email', 'LIKE', $data['email']],
            ['token', 'LIKE', $data['token']]
            ])->delete()) {
            if($this->setNewUserPass($data['email'], $data['password'])){    
            return $this->successResponse(true, 200);}
        }
        else {
            return $this->errorResponse('Reset token not valid', 403);
        }
    } 
}

private function setNewUserPass(string $email, string $password, string $userinfoid='') {
    $user=null;
    if($email){$user = User::whereEmail($email)->first();}
    if($userinfoid){$user = User::where('idUserInfo','like',$userinfoid)->first();}
    if($user) {
           if($user->update(['password'=>bcrypt($password)])){
               return true;
           };
    }
    return false;
}


// Create url with token and send it back to user via EMAIL
public function reqPassRes(Request $request){
    $data = $request->all();
    $errors = $this->apiResetReqbvalidator($data)->errors();
    if (count($errors)){
        return $this->successResponse($errors, 403);
    }
    else {
        $user = User::where('email', $data['email'])->first();
        if ($user){
            $token = $this->setResetToken($data['email']);
            $this->sendEmail($data['email'], $token);
            return $this->successResponse($data, 200);
       }
    }
}
//

// Create resetPass Token
protected function setResetToken(string $email) {
    $oldToken = DB::table('password_resets')->where('email', $email)->first();    
    if (!$oldToken){
    $token = str_random(60);
    DB::table('password_resets')->insert([
        'email' => $email,
        'token' => $token,
        'created_at' => Carbon::now()
    ]);
    return $token;} else { return $oldToken->token;}

}
//

protected function sendEmail(string $email, string $token = '') {
    return $this->successResponse($email,200); 
    // if($token !=='') {
        // Mail::to($email)->send(new ResetPassMail($token)); 
        // }
    }

    // Login data validation
    protected function apiResetPassValidator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6|confirmed',
            'token' => 'required|string|min:60'
        ]);
    }

        // Login data validation
        protected function apiLoginValidator(array $data)
        {
            return Validator::make($data, [
                'email' => 'required|string|email|max:255',
                'password' => 'required|string|min:6'
            ]);
        }

    // Password request email validator
    protected function apiResetReqbvalidator(array $data) {
        return Validator::make($data, [
            'email' => 'required|string|email|max:255'
        ]);
    }

}
