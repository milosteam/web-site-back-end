<?php

namespace App\Http\Controllers\Link;

use Exception;
use App\Model\Link\Link;
use App\Model\Text\Text;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use App\Http\Controllers\ApiController;
use phpDocumentor\Reflection\Types\Boolean;
use App\Http\Requests\Link\StoreLinkRequest;

class LinkController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $pag = $this->paginationVal($request);
            $page = $pag['page'];
            Paginator::currentPageResolver(function () use ($page) {
                return $page;
            });
            $header = $request->query();
            if (isset($header['filter'])) {
                $filt = json_decode($header['filter'], true);
                $query = $this->filterQuery($filt);
                if ($pag) {
                    return ($this->successResponse($query->orderBy('updated_at', 'DESC')->with('texts')->paginate($pag['per_page']), 200));
                } else {
                    return ($this->successResponse($query->orderBy('updated_at', 'DESC')->with('texts')->paginate($pag['per_page']), 200));
                }
            } else {
                if ($pag) {
                    $links = LINK::orderBy('updated_at', 'DESC')->with('texts')->paginate($pag['per_page']);
                } else {
                    $links = LINK::orderBy('updated_at', 'DESC')->with('texts')->get();
                }
                return ($this->successResponse($links, 200));
            }
        } catch (Exception $e) {
            return $this->errorResponse('Dogodila se greška, pokušajte ponovo.' . $e, 403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLinkRequest $request)
    {
        try {
            $request->validated();
            $data = $request->all();
            if (isset($data['paragraphs'])) {
                $link = Link::create($data);
                foreach ($data['paragraphs'] as $line) {
                    if (isset($line['id_language']) && isset($line['title_par']) && isset($line['text_type'])) {
                        $line['text_paragraphs'] = json_encode(['tilte_par' => $line['title_par'], 'text' => $line['text']]);
                        $text = Text::create($line);
                        $data['text'][] = $text->id_text;
                    }
                }
                $link->texts()->attach($data['text'], ['parentGroup' => Link::LINKDATA['filePref']]);
                return $this->successResponse("Uspešeno ste dodali novi link.", 200);
            }
        } catch (Exception $e) {
            return $this->errorResponse("Došlo je do greške" . $e, 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreLinkRequest $request, $id)
    {
        $request->validated();
        $link = Link::with('texts')->findOrFail($id);
        $data = $request->all();
        $hasChanged = false;
        if ($data['paragraphs']) {
            foreach ($data['paragraphs'] as $line) {
                isset($line['id_text']) ? $hasChanged = $this->updateLineText($line) : $data['text'][] = $this->createLineText($line);
            }
            if (isset($data['text']) && count($data['text']) > 0) {
                $link->texts()->attach($data['text'], ['parentGroup' => Link::LINKDATA['filePref']]);
                $hasChanged !== true ? $hasChanged = true : false;
            }
            foreach ($data as $key => $value) { // Cheks for uodates in request compared to database 
                if (array_key_exists($key,  $link->toArray()) &&  $link->$key !== $value) {
                    $link->$key = $value;
                    $hasChanged !== true ? $hasChanged = true : false;
                }
            }
            if (!$hasChanged) {
                return $this->errorResponse('Nista od podataka nije izmenjeno', 422);
            } else {
                $link->isDirty() ? $link->save() : false;
                return $this->successResponse('Uspešno ste izmenili plan / merenje', 200);
            }
        }
    }

    public function removeLinkText(Request $request, $id)
    {
        $link = Link::with('texts')->findOrFail($id);
        $id_text = $request->all();
        return $this->_removeLinkText($link, $id_text['id_text']) ?
            $this->successResponse('Uspešno ste uklonili tekst', 200) :
            $this->errorResponse('Uspešno ste uklonili tekst', 200);
    }

    public function getMenuLinks(Request $request, $menuid)
    {
            
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function createLineText($line)
    {
        if (isset($line['id_language']) && isset($line['title_par']) && isset($line['text_type'])) {
            $line['text_paragraphs'] = json_encode(['tilte_par' => $line['title_par'], 'text' => $line['text']]);
            $text = Text::create($line);
            return $text->id_text;
        }
    }

    private function updateLineText($line)
    {
        $linepar = json_encode(['tilte_par' => $line['title_par'], 'text' => $line['text']]);
        $dbline = Text::findOrFail($line['id_text']);
        if ($linepar !== $dbline['text_paragraphs']) {
            $dbline->text_paragraphs = $linepar;
            $dbline->save();
            return true;
        }
    }

    private function _removeLinkText(Link $link, Int $id_text)
    {
        $link->texts()->detach($id_text);
        $text = Text::findOrFail($id_text);
        $text->delete();
        return true;
    }
}
