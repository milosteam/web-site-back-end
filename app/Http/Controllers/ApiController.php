<?php

namespace App\Http\Controllers;

use App\Traits\ApiResponser;
use App\Traits\FileUploader;
use Illuminate\Http\Request;
use App\Traits\PaginationRequest;

class ApiController extends Controller
{
    use PaginationRequest;
    use FileUploader;
    use ApiResponser;
}
