<?php

namespace App\Http\Controllers\Text;

use Exception;
use App\Fileupload;
use App\Model\Text\Text;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\Text\TextFilter;
use App\Http\Requests\Text\StoreTextRequest;

class TextController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $pag = $this->paginationVal($request);
            $page = $pag['page'];
            Paginator::currentPageResolver(function () use ($page) {
                return $page;
            });
            $header = $request->query();
            if (isset($header['filter'])) {
                $filt = json_decode($header['filter'], true);
                $query = $this->filterQuery($filt);
                if ($pag) {
                    return ($this->successResponse($query->orderBy('updated_at', 'DESC')->with('files')->paginate($pag['per_page']), 200));
                } else {
                    return ($this->successResponse($query->orderBy('updated_at', 'DESC')->with('files')->paginate($pag['per_page']), 200));
                }
            } else {
                if ($pag) {
                    $texts = Text::orderBy('updated_at', 'DESC')->with('files')->paginate($pag['per_page']);
                } else {
                    $texts = Text::orderBy('updated_at', 'DESC')->with('files')->get();
                }
                return ($this->successResponse($texts, 200));
            }
        } catch (Exception $e) {
            return $this->errorResponse('Dogodila se greška, pokušajte ponovo.' . $e, 403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTextRequest $request)
    {
        try {
            $request->validated();
            $data = $request->all();
            $text = Text::create($data);
            $filesSqlData = $this->multyFileUpload($request, Text::TEXTDATA);
            if ($filesSqlData && $filesSqlData[0]) {
                foreach ($filesSqlData as $sqldata) {
                    $file = new Fileupload($sqldata);
                    if ($file->save()) {
                        $data['files'][] = $file->idFile;
                    }
                }
                $text->files()->attach($data['files'], ['parentGroup' => TEXT::TEXTDATA['filePref']]);
            }
            return $this->successResponse('Uspešno ste dodali novi tekst.', 200);
        } catch (Exception $e) {
            return $this->errorResponse("Došlo je do greške" . $e, 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreTextRequest $request, $id)
    {
        $request->validated();
        $text = Text::findOrFail($id);
        if ($request->hasFile('katImage')) { //If switched to rich text for image upload
            $filesSqlData = $this->multyFileUpload($request, Text::TEXTDATA);
            if ($filesSqlData && $filesSqlData[0]) {
                foreach ($filesSqlData as $sqldata) {
                    $file = new Fileupload($sqldata);
                    if ($file->save()) {
                        $data['files'][] = $file->idFile;
                    }
                }
                $text->files()->attach($data['files'], ['parentGroup' => TEXT::TEXTDATA['filePref']]);
            }
        }
        $data = $request->all();
        foreach ($data as $key => $value) { // Cheks for uodates in request compared to database 
            if (array_key_exists($key,  $text->toArray()) &&  $text->$key !== $value) {
                $text->$key = $value;
            }
        }
        if (!$text->isDirty()) {
            return $this->errorResponse('Nista od podataka nije izmenjeno', 422);
        } else {
            $text->save();
            return $this->successResponse('Uspešno ste izmenili plan / merenje', 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function filterQuery(array $filter)
    {
        $uredjaj = new Text();
        $filt = new TextFilter();
        $uredjaj = $uredjaj->newQuery();
        $where = [];
        foreach ($filter as $value) {
            foreach ($value as $key => $value) {
                $where[] = $filt->filter($key, $value);
            }
        }

        if (count($filter) === 1) {
            return $uredjaj->where($where[0][0], $where[0][1], $where[0][2]);
        }

        if (count($filter) === 2) {
            return $uredjaj->where($where[0][0], $where[0][1], $where[0][2])
                ->where($where[1][0], $where[1][1], $where[1][2]);
        }

        if (count($filter) === 3) {
            return $uredjaj->where($where[0][0], $where[0][1], $where[0][2])
                ->where($where[1][0], $where[1][1], $where[1][2])
                ->where($where[2][0], $where[2][1], $where[2][2]);
        }

        if (count($filter) === 4) {
            return $uredjaj->where($where[0][0], $where[0][1], $where[0][2])
                ->where($where[1][0], $where[1][1], $where[1][2])
                ->where($where[2][0], $where[2][1], $where[2][2])
                ->where($where[3][0], $where[3][1], $where[3][2]);
        }
        if (count($filter) === 5) {
            return $uredjaj->where($where[0][0], $where[0][1], $where[0][2])
                ->where($where[1][0], $where[1][1], $where[1][2])
                ->where($where[2][0], $where[2][1], $where[2][2])
                ->where($where[3][0], $where[3][1], $where[3][2])
                ->where($where[4][0], $where[4][1], $where[4][2]);;
        }
    }
}
