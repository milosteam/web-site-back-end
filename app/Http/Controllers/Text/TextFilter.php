<?php 
namespace App\Http\Controllers\Text;

class TextFilter {
    private static $filterKeys=[
        'id_text',
        'text_type',
        'text_name',
        'text_title',
    ]; 

    public function filter($key, $value) {

        if (in_array($key, $this::$filterKeys)){
            return $this->$key($value);
        }        
    }
    protected function id_text($value){
      return array($this::$filterKeys[0],'=', $value);    
    }

    protected function text_type($value){
        return array($this::$filterKeys[1], 'LIKE', $value); 
    }

    protected function text_name($value){
        return array($this::$filterKeys[2], 'LIKE', $value.'%'); 
    }

    protected function text_title($value) {
        return array($this::$filterKeys[3], 'LIKE', $value.'%');
    }
}


?>