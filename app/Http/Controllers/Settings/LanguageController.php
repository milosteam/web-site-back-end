<?php

namespace App\Http\Controllers\Settings;

use Exception;
use App\Fileupload;
use App\Traits\FileUploader;
use Illuminate\Http\Request;
use App\Model\Settings\Language;
use App\Traits\PaginationRequest;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Settings\Languages\Storelangreq;

class LanguageController extends ApiController
{

    use PaginationRequest;
    use FileUploader;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $pag = $this->paginationVal($request);
            $page = $pag['page'];
            Paginator::currentPageResolver(function () use ($page) {
                return $page;
            });
            $header = $request->query();
            if (isset($header['filter'])) {
                $filt = json_decode($header['filter'], true);
                $query = $this->filterQuery($filt);
                if ($pag) {
                    return ($this->successResponse($query->orderBy('updated_at', 'DESC')->with('files')->paginate($pag['per_page']), 200));
                } else {
                    return ($this->successResponse($query->orderBy('updated_at', 'DESC')->with('files')->paginate($pag['per_page']), 200));
                }
            } else {
                if ($pag) {
                    $langauges = Language::orderBy('updated_at', 'DESC')->with('files')->paginate($pag['per_page']);
                } else {
                    $langauges = Language::orderBy('updated_at', 'DESC')->with('files')->get();
                }
                return ($this->successResponse($langauges, 200));
            }
        } catch (Exception $e) {
            return $this->errorResponse('Greska' . $e, 403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Storelangreq $request)
    {
        $request->validated();
        $data = $request->all();
        $langauge = Language::create($data);
        $filesSqlData = $this->multyFileUpload($request, Language::LANGUGAEDATA);
        if ($filesSqlData && $filesSqlData[0]) {
            foreach ($filesSqlData as $sqldata) {
                $file = new Fileupload($sqldata);
                if ($file->save()) {
                    $data['files'][] = $file->idFile;
                }
            }
            $langauge->files()->attach($data['files'], ['parentGroup' => Language::LANGUGAEDATA['filePref']]);
        }
        return $this->successResponse($filesSqlData, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Storelangreq $request, $id)
    {
        $request->validated();
        $language= Language::findOrFail($id);
        $data = $request->all();
        foreach ($data as $key => $value) {
            if (array_key_exists($key,  $language->toArray()) &&  $language->$key !== $value) {
                 $language->$key = $value;
            }
        }
        if (! $language->isDirty()) {
            return $this->errorResponse( 'Nista od podataka nije izmenjeno', 422);
        } else {
             $language->save();
            return $this->successResponse('Uspešno ste izmenili plan / merenje', 200);
            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
