<?php

namespace App\Http\Controllers\Proxy;

use App\User;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Foundation\Application;
use Symfony\Component\HttpFoundation\Cookie;



// use Infrastructure\Auth\Exceptions\InvalidCredentialsException;

class LoginProxy
{

    use ApiResponser;

    const REFRESH_TOKEN = 'refreshToken';

    private $apiConsumer;

    private $auth;

    private $db;

    private $request;

    private $userRepository;

    public function __construct(Application $app) {
        $this->apiConsumer = $app->make('apiconsumer');
        $this->auth = $app->make('auth');
        $this->db = $app->make('db');
        $this->request = $app->make('request');
    }

    public function attemptLogin($email, $password)
    {
        $user = User::where('email', $email)->first();
        if (!is_null($user)) {
            return $this->proxy('password', [
                'username' => $email,
                'password' => $password,
            ]);
        }

      return $this->errorResponse('Uneti podaci nisu validni attemtLogin', 403);
    }

    public function attemptRefresh($request)
    {
        $refreshToken = $request->cookie(self::REFRESH_TOKEN);

        return $this->proxy('refresh_token', [
            'refresh_token' => $refreshToken
        ]);
    }

    public function proxy($grantType, array $data = [])
    {
        $data = array_merge($data, [
            'client_id'     => env('PASSPORT_PASSWORD_ID'),
            'client_secret' => env('PASSPORT_PASSWORD_SECRET'),
            'grant_type'    => $grantType
        ]);
        $response = $this->apiConsumer->post('/oauth/token', $data);
        if (!$response->isSuccessful()) {
            return $this->errorResponse($data, 401);
        }

        $data = json_decode($response->getContent());

        //   Create a refresh token cookie
        $cookie = Cookie::create(
            self::REFRESH_TOKEN,
            $data->refresh_token,
            86400, // 10 days,
            "/",
            null,
            null,
            false // HttpOnly
        );

        return [
            'access_token' => $data->access_token,
            'expires_in' => $data->expires_in,
            'cookie' => $cookie
        ];

    }


    public function logout()
    {
        $accessToken = $this->auth->user()->token();

        $refreshToken = $this->db
            ->table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update([
                'revoked' => true
            ]);

        $accessToken->revoke();

        $this->cookie->queue($this->cookie->forget(self::REFRESH_TOKEN));
    }
}

