<?php

namespace App\Http\Controllers\Fileupload;

use App\Delovi;
use App\Oprema;
use App\Uredjaj;
use App\Fileupload;
use App\Model\Text\Text;
use App\Traits\FileUploader;
use Illuminate\Http\Request;
use App\Oprema\Sklopovi\Sklop;
use League\Flysystem\Exception;
use App\Model\Settings\Language;
use App\Http\Controllers\Controller;
use function GuzzleHttp\json_decode;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;

class FileUploadController extends ApiController
{
    use FileUploader;

    public function removePivotImage(Request $request)
    {
        try {
            $data = $request->all(); //Getting request data
            if (isset($data['file']) && isset($data['file']['directoryOfFile'])) { // IF file data is present
                $model = $data['file']['directoryOfFile'];
                $fileData = $this->setFormData($model); // Depending on the perent of the image getting data
                $this->deleteFile($fileData, $data['file']['nameOfFile']); // Deleting file form server           
                $file = Fileupload::where('idFile', '=', $data['file']['idFile'])->first(); //Getting file from DB
                $file->$model()->detach(); // Detaching many to many relationship
                $deletedRows = $file->delete(); // Deletetin file in DB
                return $this->successResponse(true, 200);
            } else {
                return $this->errorResponse(false, 403);
            }
        } catch (Exception $e) {
            return $this->errorResponse('Došlo je do greške, pokušajte ponovo' . $e, 403);
        }
    }

    public function addPivotImage(Request $request)
    {
        try {
            $data = $request->all();
            if (isset($data['grupa'])) {
                $data['grupa'] = json_decode($data['grupa'], true);
                $model =  $data['grupa']['parent'];
                $fileData = $this->setFormData($model);
                $fileSqlData = $this->filesAndData($request, $fileData);
                $file = new FileUpload($fileSqlData);
                if ($file->save()) {
                    $file->$model()->attach($data['grupa']['id_' . $model], ['parentGroup' => $fileData['filePref']]);
                    return ($this->successResponse($file, 200));
                }
            }
        } catch (Exception $e) {
            return $this->errorResponse('Došlo je do greške, pokušajte ponovo' . $e, 403);
        }
    }

    public function addSeoImage(Request $request)
    {
        $data = $request->all();
        // if (isset($data['imagesarry']) && is_array($data['imagesarry'])) {
        //     foreach ($data['imagesarry'] as $image) {
                
        //     }
        // }
        return $this->successResponse($data, 200);
    }

    function getFileFromStorage($file)
    {
        try {
            $filepath = Fileupload::where('idFile', '=', $file)->first();
            $file = storage_path('app/public/' . $filepath->desktopThumbPath);
            $path = storage_path('app/public/images/defaultsmall.gif');
            if (file_exists($file)) {
                return response()->download($file);
            } else {
                return response()->download($file);
            }
        } catch (Exception $e) {
            return response()->download($path);
        }
    }


    private function setFormData(String $grupa)
    {
        switch ($grupa) {
            case 'text':
                return Text::TEXTDATA;
                break;
            case 'language':
                return Language::LANGUGAEDATA;
                break;
            default:
                throw new Exception('File data is not defined', 403);
        }
    }
    //
}
