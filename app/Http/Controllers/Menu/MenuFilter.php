<?php 
namespace App\Http\Controllers\Menu;

class MenuFilter {
    private static $filterKeys=[
        'id_menu',
        'menu_name',
        'menu_position',
    ]; 

    public function filter($key, $value) {

        if (in_array($key, $this::$filterKeys)){
            return $this->$key($value);
        }        
    }
    protected function id_menu($value){
      return array($this::$filterKeys[0],'=', $value);    
    }

    protected function menu_name($value){
        return array($this::$filterKeys[1], 'LIKE', $value); 
    }

    protected function menu_position($value){
        return array($this::$filterKeys[2], 'LIKE', $value.'%'); 
    }
}


?>