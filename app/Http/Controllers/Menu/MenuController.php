<?php

namespace App\Http\Controllers\Menu;

use Exception;
use App\Model\Link\Link;
use App\Model\Menu\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Menu\StoreMenuRequest;

class MenuController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $pag = $this->paginationVal($request);
            $page = $pag['page'];
            Paginator::currentPageResolver(function () use ($page) {
                return $page;
            });
            $header = $request->query();
            if (isset($header['filter'])) {
                $filt = json_decode($header['filter'], true);
                $query = $this->filterQuery($filt);
                if ($pag) {
                    return ($this->successResponse($query->orderBy('updated_at', 'DESC')->with('links')->paginate($pag['per_page']), 200));
                } else {
                    return ($this->successResponse($query->orderBy('updated_at', 'DESC')->with('links')->paginate($pag['per_page']), 200));
                }
            } else {
                if ($pag) {
                    $menus = Menu::with('links')->orderBy('updated_at', 'DESC')->paginate($pag['per_page']);
                } else {
                     $menus = Menu::with('links')->orderBy('updated_at', 'DESC')->get();
                }
                return $this->successResponse($menus, 200);
            }
        } catch (Exception $e) {
            return $this->errorResponse('Dogodila se greška, pokušajte ponovo.' . $e, 403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMenuRequest $request)
    {
        $request->validated();
        $data = $request->all();
        $menu = Menu::create($data);
        if (isset($data['links'])) {
            foreach ($data['links'] as $link) {
                $menu->links()->attach(
                    $link['id_link'],
                    [
                        'position' => $link['position'],
                        'level' => $link['level'],
                        'id_parent_link' => $link['id_parent_link']
                    ]
                );
            }
        }

        return $this->successResponse($menu, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function createNestedMenu($links)
    {
        $nested = array();
        foreach ($links as $link) {
            if ($link['pivot']['level'] === 0) {
                $nested[$link['id_link']] = $link;
            }
        }
        return $this->successResponse($nested, 200);
    }

    public function filterQuery(array $filter)
    {
        $menu = new Menu();
        $filt = new MenuFilter();
        $menu = $menu->newQuery();
        $where = [];
        foreach ($filter as $value) {
            foreach ($value as $key => $value) {
                $where[] = $filt->filter($key, $value);
            }
        }

        if (count($filter) === 1) {
            return $menu->where($where[0][0], $where[0][1], $where[0][2]);
        }

        if (count($filter) === 2) {
            return $menu->where($where[0][0], $where[0][1], $where[0][2])
                ->where($where[1][0], $where[1][1], $where[1][2]);
        }

        if (count($filter) === 3) {
            return $menu->where($where[0][0], $where[0][1], $where[0][2])
                ->where($where[1][0], $where[1][1], $where[1][2])
                ->where($where[2][0], $where[2][1], $where[2][2]);
        }

        if (count($filter) === 4) {
            return $menu->where($where[0][0], $where[0][1], $where[0][2])
                ->where($where[1][0], $where[1][1], $where[1][2])
                ->where($where[2][0], $where[2][1], $where[2][2])
                ->where($where[3][0], $where[3][1], $where[3][2]);
        }
        if (count($filter) === 5) {
            return $menu->where($where[0][0], $where[0][1], $where[0][2])
                ->where($where[1][0], $where[1][1], $where[1][2])
                ->where($where[2][0], $where[2][1], $where[2][2])
                ->where($where[3][0], $where[3][1], $where[3][2])
                ->where($where[4][0], $where[4][1], $where[4][2]);;
        }
    }
}
