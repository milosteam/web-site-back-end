@component('mail::message')
# PROMENA PRISTUPNE ŠIFRE

Klikom na dugme ispod bićete redirektovani na formu za podešavanje nove pristupne šifre.
Autorizacija za podešavanje nove pristupne ističe nakon 5 minuta.

@component('mail::button', ['url' => 'http://localhost:4200/#/respass?token='.$token])
NOVA PRISTUPNA ŠIFRA
@endcomponent

Uspešan dan,<br>
{{ config('app.name') }}
@endcomponent
